;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (guixgui main)
  #:use-module ((guix packages) #:prefix packages:)
  #:use-module ((guix profiles) #:prefix profiles:)
  #:use-module (oop goops)
  #:use-module (gi)
  #:use-module (gi repository)
  #:use-module (gi types)
  #:use-module (srfi srfi-26) ; <>
  #:use-module (srfi srfi-19) ; date->string
  #:use-module ((srfi srfi-1) #:select (take-while drop-while filter-map))
  #:use-module (srfi srfi-11) ; let-values
  #:use-module (srfi srfi-43) ; vector-map, vector-for-each
  #:use-module (ice-9 match)
  #:use-module ((guixgui manifests) #:prefix gui-manifests:)
  #:use-module ((guixgui package-filters) #:prefix gui-package-filters:))

(define G_PRIORITY_DEFAULT_IDLE 200) ; FIXME import from glib

(use-typelibs (("GLib" "2.0") #:select (idle-add)))
(use-typelibs (("Gtk" "3.0") #:select (symbol->window-type symbol->orientation symbol->sort-type symbol->selection-mode symbol->policy-type)))

(eval-when (compile load eval)
  (require "GObject" "2.0")
  ;(load-by-name "GObject" "Object") ; https://github.com/spk121/guile-gi/issues/88
  ;(require "GdkPixbuf" "2.0")
  (require "Gio" "2.0")
  (require "Gtk" "3.0")
  (load-by-name "Gio" "Application")
  (load-by-name "Gio" "ApplicationFlags")
  (load-by-name "GObject" "Value")

  (for-each
   (cute load-by-name "Gtk" <>)
   '("ApplicationWindow" "Application" "Container" "Window" "Widget" "TreeView" "TreeStore" "TreeModel" "TreeModelSort" "TreeIter" "TreeView" "TreeViewColumn" "TreeSortable" "CellRendererText" "CellRenderer" "ScrolledWindow" "Builder" "ButtonBox" "Button" "HeaderBar" "Tooltip" "Dialog" "Box" "TreeSelection" "Expander" "Label" "TreeModelFilter" "TreePath" "TreeIter" "TreeModelFilter")))

(define tree-model:iter-previous! tree-model:iter-previous?) ; work around <https://github.com/spk121/guile-gi/issues/87>.
(define tree-model:iter-next! tree-model:iter-next?) ; work around <https://github.com/spk121/guile-gi/issues/87>.
(define tree-store:remove! tree-store:remove?) ; work around <https://github.com/spk121/guile-gi/issues/87>.
(define tree-view:collapse-row tree-view:collapse-row?)

(define (tree-model-cell-value model iter index)
  (let ((result (make <GValue>)))
    (tree-model:get-value! model iter index result)
    (result)))

(define (tree-model-set-cell-value! model iter index value)
  (define row (list->vector (map (lambda (index)
                                   (value:init (make <GValue>) <string>))
                                 (iota 1))))
  (set! ((vector-ref row 0) <string>) value)
  (tree-store:set model iter (u32vector index) row))

(define (tree-model-iter tree-model tree-path)
  (let ((result (make <GtkTreeIter>)))
    (tree-model:get-iter! tree-model result tree-path)
    result))

(define (tree-model-filter-new tree-model root-path)
  (make <GtkTreeModelFilter> #:child-model tree-model #:virtual-root root-path))

(define (tree-model-iter-parent tree-model tree-iter)
  (let ((parent (make <GtkTreeIter>)))
    (if (tree-model:iter-parent! tree-model parent tree-iter)
        parent
        #f)))

(define (tree-model-iter-previous tree-model tree-iter)
  (let ((result (tree-iter:copy tree-iter)))
    (if (tree-model:iter-previous! tree-model result)
        result
        #f)))

(define (tree-model-iter-next tree-model tree-iter)
  (let ((result (tree-iter:copy tree-iter)))
    (if (tree-model:iter-next! tree-model result)
        result
        #f)))

(define (tree-model-iter-children tree-model parent-iter)
  (let ((iter (make <GtkTreeIter>)))
    (if (tree-model:iter-children! tree-model iter parent-iter)
        iter
        #f)))

;(define (tree-model-fold tree-model iter kons knil)
;  "Starting at ITER, collect it and go to the next siblings, doing the same"
;  (let loop ((iter iter))
;    (if iter
;        (kons iter (loop (tree-model-iter-next tree-model iter)))
;        knil)))

;; Note: Not recursive.
;(define (tree-model-fold-children tree-model parent-iter kons knil)
;  (tree-model-fold tree-model (tree-model-iter-children tree-model parent-iter) kons init))

(define (tree-model-foreach-sibling tree-model iter callback)
  (let loop ((iter iter))
    (when iter
        (callback iter)
        (loop (tree-model-iter-next tree-model iter)))))

(define (tree-selection-get-selected-rows selection)
  (let-values (((paths model)
                (tree-selection:get-selected-rows selection)))
    paths))

(define (tree-filter-iter->store-iter tree-filter-model tree-iter)
  (let ((result (make <GtkTreeIter>)))
    (tree-model-filter:convert-iter-to-child-iter! tree-filter-model result tree-iter)
    result))

(define (tree-store-iter->filter-iter tree-filter-model tree-iter)
  (let ((result (make <GtkTreeIter>)))
    (tree-model-filter:convert-child-iter-to-iter! tree-filter-model result tree-iter)
    result))

(define (tree-model-iter-nth-child tree-store parent-iter n)
  (let ((result (make <GtkTreeIter>)))
    (if (tree-model:iter-nth-child! tree-store result parent-iter n)
        result
        #f)))

(define (tree-model-row-package model iter)
  "Return a package representant of ITER if ITER represents a package.  Otherwise return #f."
  (let* ((name (tree-model-cell-value model iter gui-manifests:name-column-index))
         (version (tree-model-cell-value model iter gui-manifests:version-column-index)))
    (if name
        (gui-manifests:package-representant name version)
        #f)))
;    (write "tree-model-row-package")
;    (write iter)
;    (write (vector-map (lambda (i x) (x)) (gui-manifests:tree-model-row model iter)))
;    (write name)
;    (write version)
;    (newline)
;    package-representant)

(define (install-tree-view-tooltip-handler! tree-view tree-filter-model tree-store)
  (set! (has-tooltip tree-view) #t)
  (connect tree-view query-tooltip
   (lambda (tree-view x y keyboard-tip tooltip)
     (let* ((iter (make <GtkTreeIter>)))
       (let-values (((on-row? x y model path iter)
                     (tree-view:get-tooltip-context! tree-view x y keyboard-tip iter)))
         (if (and on-row? model)
             (begin
               (let ((tooltip-text
                      (match (tree-model-cell-value model iter gui-manifests:tooltip-column-index)
                       ("" #f)
                       (x x)))
                     (cache-tooltip-text
                      (lambda ()
                        (let* ((package (tree-model-row-package model iter))
                               ;(package (or (value:get-boxed
                               ;              (tree-model-cell-value model iter gui-manifests:package-column-index))
                               ;             package-representant))
                               (tooltip-text (and=> package gui-manifests:package->tooltip)))
                         ;; Cache the value.
                         (when tooltip-text
                           (tree-model-set-cell-value! tree-store
                                                       (tree-filter-iter->store-iter tree-filter-model iter)
                                                       gui-manifests:tooltip-column-index
                                                       tooltip-text))
                         tooltip-text))))
                 (tooltip:set-markup tooltip (or tooltip-text (cache-tooltip-text) ""))
                 (tree-view:set-tooltip-row tree-view tooltip path)
                 (and tooltip-text #t)))
             #f))))))

(define (tree-view-add-text-column! tree-view title model-column-index)
  (let ((cell-renderer (cell-renderer-text:new))
        (tree-column-0 (tree-view-column:new)))
    (tree-view-column:set-title tree-column-0 title)
    (tree-view-column:pack-start tree-column-0 cell-renderer #t)
    (tree-view-column:add-attribute tree-column-0 cell-renderer "text" model-column-index)
    (tree-view:append-column tree-view tree-column-0)))

(define (tree-view-new-with-simple-model tree-model root-path display-name-title display-name-column-index tooltip-column-index)
  "Create a new, unscollable, tree view with a filter model of TREE-MODEL, starting at
ROOT-PATH (a <GtkTreePath> or #f).  The tree view will have one column with title
DISPLAY-NAME-TITLE.  The displayed strings in the cells will be taken from the model
column with index DISPLAY-NAME-COLUMN-INDEX.  The tooltips will be taken from the model
column with index TOOLTIP-COLUMN-INDEX."
  (let ((tree-view (tree-view:new))
        (tree-filter-model (tree-model-filter-new tree-model root-path)))
    ;(set-tooltip-column tree-view tooltip-column-index)
    (install-tree-view-tooltip-handler! tree-view tree-filter-model tree-model)
    (tree-selection:set-mode (tree-view:get-selection tree-view) (symbol->selection-mode 'multiple))
    (tree-sortable:set-sort-column-id tree-model display-name-column-index (symbol->sort-type 'ascending))
    (tree-view-add-text-column! tree-view display-name-title display-name-column-index)
    (tree-view:set-model tree-view tree-filter-model)
    (tree-view:set-search-column tree-view display-name-column-index)
    (values tree-view tree-filter-model)))

(define (scrollable-tree-view-new-with-simple-model tree-model root-path display-name-title display-name-column-index tooltip-column-index)
  "Create a new ScrolledWindow with a tree view with a filter model of TREE-MODEL, starting at
ROOT-PATH (a <GtkTreePath> or #f).  The tree view will have one column with title
DISPLAY-NAME-TITLE.  The displayed strings in the cells will be taken from the model
column with index DISPLAY-NAME-COLUMN-INDEX.  The tooltips will be taken from the model
column with index TOOLTIP-COLUMN-INDEX."
  (let-values (((tree-view tree-filter-model)
                (tree-view-new-with-simple-model tree-model root-path display-name-title display-name-column-index tooltip-column-index)))
    (let ((scrolled-window (scrolled-window:new)))
      (scrolled-window:set-policy scrolled-window (symbol->policy-type 'never) (symbol->policy-type 'automatic))
      (add scrolled-window tree-view)
      (values scrolled-window tree-view tree-filter-model))))

(define (tree-window-new-with-simple-model window-title tree-model display-name-title display-name-column-index tooltip-column-index)
  (let ((window (window:new (symbol->window-type 'toplevel))))
    (let-values (((scrolled-window tree-view tree-filter-model)
                  (scrollable-tree-view-new-with-simple-model tree-model #f display-name-title display-name-column-index tooltip-column-index)))
      (window:set-title window window-title)
      (add window scrolled-window))
  window))

(define (dialog-add-buttons dialog buttons-and-responses)
  (for-each
    (match-lambda
     ((text response)
      (dialog:add-button dialog text response)))
    buttons-and-responses)
  dialog)

(define (dialog-new-with-buttons title parent flags . buttons-and-responses)
  (let ((dialog (make <GtkDialog>
                 #:use-header-bar (if (member 'use-header-bar flags)
                                      1
                                      0))))
    (set-title dialog title)
    (set-transient-for dialog parent)
    (when (member 'modal flags)
      (set-modal dialog #t))
    (when (member 'destroy-with-parent flags)
      (set-destroy-with-parent dialog #t))
    (dialog-add-buttons dialog buttons-and-responses)
    dialog))

;; TODO: Split available-packages-search-panel-new, but allow it to provide actions for the parent dialog.
(define (available-packages-search-dialog-run title parent proceeding-button-text proceeding-button-action)
  (let* ((dialog (dialog-new-with-buttons title parent '(modal) `("_Cancel" ,2) `(,proceeding-button-text ,1)))
         (panel (box:new (symbol->orientation 'vertical) 0))
         (store gui-manifests:available-packages))
    (let-values (((scrolled-window tree-view tree-filter-model)
                 (scrollable-tree-view-new-with-simple-model store #f "Package" gui-manifests:display-name-column-index gui-manifests:tooltip-column-index)))
      (set-default-response dialog 1)
      (pack-start panel (gui-package-filters:package-filters tree-filter-model) #f #f 7)
      (pack-start panel scrolled-window #t #t 7)
      (pack-start (get-content-area dialog) panel #t #t 7)
      (show-all panel)
      (let ((result (run dialog)))
        (hide dialog)
        ;; TODO: destroy dialog.
        result))))

(define* (profile-generation-action-buttons-new profile generation-number)
  "Create action buttons for PROFILE.  GENERATION-NUMBER is used as a hint on
which generation the user is currently viewing."
  (let* ((box (button-box:new (symbol->orientation 'vertical)))
         (package-installation-button
          (button:new-with-mnemonic "_Install package..."))
         (package-uninstallation-button
          (button:new-with-mnemonic "_Uninstall packages"))
         (previous-generation-opening-button
          (button:new-with-mnemonic "Open previous generation"))
         (next-generation-opening-button
          (button:new-with-mnemonic "Open next generation"))
         (committal-button
          (button:new-with-mnemonic "_Commit"))
         (generations
          (profiles:profile-generations profile))
         (current-generation-number
          (profiles:generation-number profile)))
    (set! (sensitive previous-generation-opening-button)
     (if generation-number
         (match generations
          ((first-generation rest ...)
           (not (= first-generation generation-number)))
          (_ #f))
         #t))
    (set! (sensitive next-generation-opening-button)
     (if generation-number
         (match (member generation-number generations)
          ((first-generation next-generation rest ...) #t)
          (_ #f))
         #t))
    (set! (sensitive package-installation-button)
     (or (not generation-number)
         (= generation-number current-generation-number)))
    (set! (sensitive package-uninstallation-button)
     (or (not generation-number)
         (= generation-number current-generation-number)))
    (set! (sensitive committal-button) #f)
    (connect package-installation-button clicked
      (lambda args
        (available-packages-search-dialog-run "Install package..." (get-toplevel box) "_Install"
         (lambda (selected-packages)
           (write "FIXME install packages")
           (write selected-packages)
           (newline)))))
    (connect previous-generation-opening-button clicked
      (lambda args
        (let ((previous-generation-number
               (match (take-while
                       (lambda (x-generation-number)
                         (not (= generation-number x-generation-number)))
                       generations)
                ((rest ... last-generation-number)
                 last-generation-number))))
          (show-all (profile-generation-window-new profile previous-generation-number)))))
    (connect next-generation-opening-button clicked
      (lambda args
        (let ((next-generation-number
               (match (drop-while
                       (lambda (x-generation-number)
                         (not (= generation-number x-generation-number)))
                       generations)
                ((first-generation-number next-generation-number rest ...)
                 next-generation-number))))
          (show-all (profile-generation-window-new profile next-generation-number)))))
    (box:pack-start box committal-button #t #t 0)
    (box:pack-start box package-installation-button #t #t 0)
    (box:pack-start box package-uninstallation-button #t #t 0)
    (box:pack-start box previous-generation-opening-button #t #t 0)
    (box:pack-start box next-generation-opening-button #t #t 0)
    box))

(define (profile-generation-window-new profile generation-number)
  (let* ((generation (profiles:generation-file-name profile generation-number))
         (generation-timestamp
          (date->string
           (time-utc->date
            (profiles:generation-time profile generation-number))))
         (current-generation-number
          (profiles:generation-number profile))
         (window (window:new (symbol->window-type 'toplevel)))
         (panel (box:new (symbol->orientation 'horizontal) 0))
         (generation-manifest (profiles:profile-manifest generation))
         (tree-store (gui-manifests:tree-store-new))
         (header-bar (header-bar:new)))
    (let-values (((scrolled-window tree-view tree-filter-model)
                  (scrollable-tree-view-new-with-simple-model tree-store #f "Package" gui-manifests:display-name-column-index gui-manifests:tooltip-column-index)))
      (gui-manifests:->tree-store-location! generation-manifest tree-store #f)
      (pack-start panel scrolled-window #t #t 0)
      (pack-start panel (profile-generation-action-buttons-new profile generation-number)
                        #f #f 7)
      (add window panel)
      (set! (show-close-button header-bar) #t)
      (set-titlebar window header-bar)
      (set-title window (format #f "Profile ~a generation #~a"
                         profile
                         generation-number))
      (set-subtitle header-bar (format #f "(as of ~a)~a" generation-timestamp
                                          (if (= generation-number current-generation-number)
                                              "(current generation)"
                                              "")))
      ;(set-title window (guix-ui:display-generation profile generation-number))
      (set-default-size window 600 400)
      window)))

;                        (let* ((filter-iter (make <GtkTreeIter>))
;                               (_ (tree-model-filter:convert-child-iter-to-iter! tree-filter-model filter-iter store-iter))
;                               (path (tree-model:get-path tree-filter-model filter-iter)))
;                          (tree-selection:select-path (tree-view:get-selection tree-view) path)
;                          (tree-view:expand-to-path tree-view path)
;                          (scroll-to-cell tree-view path #f #t 0.0 0.0)))))

(define (tree-store-generation-iter tree-store tree-iter)
  "Given any tree store row, determine the profile generation it belongs to."
  (let up ((store-iter tree-iter))
    (let ((up-iter (tree-model-iter-parent tree-store store-iter)))
      (if up-iter
          (up up-iter)
          store-iter))))

(define (tree-view-current-generation-iter tree-view tree-store tree-filter-model)
    (let-values (((tree-path tree-column)
                  (tree-view:get-cursor tree-view)))
      (if tree-path
          (let ((iter (tree-model-iter (tree-view:get-model tree-view) tree-path)))
            (tree-store-generation-iter tree-store (tree-filter-iter->store-iter tree-filter-model iter)))
          #f)))

(define (tree-store-expand-generation tree-store store-iter profile)
  (let* ((generation-number (tree-row-generation-number tree-store store-iter))
         (generation (profiles:generation-file-name profile generation-number))
         (generation-manifest (profiles:profile-manifest generation)))
    (when (= (tree-model:iter-n-children tree-store store-iter) 1) ; FIXME: that is not safe as a detector whether the generation is already loaded!
      (let ((dummy-node-iter (tree-model-iter-nth-child tree-store store-iter 0)))
        (gui-manifests:->tree-store-location! generation-manifest tree-store store-iter)
        (when dummy-node-iter
          (tree-store:remove! tree-store dummy-node-iter))))))

(define (tree-view-select-generation tree-view tree-store store-iter tree-filter-model retain-package-selection? profile)
  "Select generation STORE-ITER, reselecting the transformed package selection (of the previous generation) and cursor if RETAIN-PACKAGE-SELECTION?."
  ;; Remember selected packages, and the package the cursor is on.
  (let* ((selection (tree-view:get-selection tree-view))
         ;; TODO: Only transform the selected packages under THIS generation.
         (selected-packages (filter-map (lambda (tree-path)
                                          (let ((iter (tree-model-iter tree-filter-model tree-path)))
                                            (tree-selection:unselect-iter selection iter)
                                            (tree-model-row-package tree-filter-model iter)))
                                        (tree-selection-get-selected-rows selection)))
         (cursor-package (let-values (((tree-path tree-column)
                                       (tree-view:get-cursor tree-view)))
                           (if tree-path
                               (tree-model-row-package tree-filter-model (tree-model-iter tree-filter-model tree-path))
                               #f))))
      ;; Collapse current generation first.
      (let ((store-iter (tree-view-current-generation-iter tree-view tree-store tree-filter-model)))
        (when store-iter
          (let ((path (tree-model:get-path tree-filter-model
                      (tree-store-iter->filter-iter tree-filter-model store-iter))))
            (tree-view:collapse-row tree-view path))))
      ;; Expand the new generation.
      (when store-iter
        (tree-store-expand-generation tree-store store-iter profile)
        (let* ((filter-iter (tree-store-iter->filter-iter tree-filter-model store-iter))
               (path (tree-model:get-path tree-filter-model filter-iter)))
          (tree-view:expand-to-path tree-view path)
          ; Note: Should make sure that widget is realized first! (tree-view:scroll-to-point tree-view 0 0)
          (scroll-to-cell tree-view path #f #t 0.0 0.0)
          ;; Restore selection, or select sensible defaults.
          (if retain-package-selection?
              (tree-model-foreach-sibling tree-filter-model (tree-model-iter-children tree-filter-model filter-iter)
               (lambda (iter)
                 (let ((package (tree-model-row-package tree-filter-model iter)))
                   ;; If package is equal to the package the cursor was on before, select it again.
                   (when (and cursor-package (eq? package cursor-package))
                     ;; FIXME: This also changes the current selection--which we don't want.
                     (tree-view:set-cursor tree-view (tree-model:get-path tree-filter-model iter) #f #f))
                   ;(write (packages:package-name package))
                   ;(write selected-packages)
                   ;(newline)
                   ;; If package in any of the selected-packages, select it again.
;                   (when (member package selected-packages)
;                     (tree-selection:select-iter selection iter))
#t
)))
              (begin
                (tree-selection:select-path selection path)
                (tree-view:set-cursor tree-view path #f #f)))))))

(define* (profile-action-buttons-new profile tree-view tree-filter-model tree-store)
  "Create action buttons for PROFILE.  TREE-VIEW, TREE-FILTER-MODEL and TREE-STORE are
used in order to find the generation the user is currently in and to set the
generation the user is currently in."
  (let* ((box (button-box:new (symbol->orientation 'vertical)))
         (package-installation-button
          (button:new-with-mnemonic "_Install package..."))
         (package-uninstallation-button
          (button:new-with-mnemonic "_Uninstall packages"))
         (previous-generation-opening-button
          (button:new-with-mnemonic "Open previous generation"))
         (next-generation-opening-button
          (button:new-with-mnemonic "Open next generation"))
         (committal-button
          (button:new-with-mnemonic "_Commit"))
         (generations
          (profiles:profile-generations profile))
         (current-generation-number
          (profiles:generation-number profile)))
    (connect package-installation-button clicked
      (lambda args
        (available-packages-search-dialog-run "Install package..." (get-toplevel box) "_Install"
         (lambda (selected-packages)
           (write "FIXME install packages")
           (write selected-packages)
           (newline)))))
    (connect (tree-view:get-selection tree-view) changed
      (lambda (selection)
        (let* ((packages
                (filter-map (lambda (path)
                              (and=> (tree-model-iter tree-filter-model path)
                                     (lambda (iter)
                                       (tree-model-row-package tree-filter-model iter))))
                            (tree-selection-get-selected-rows selection)))
               (package-count
                 (length packages)))
          (button:set-label package-uninstallation-button (format #f "_Uninstall ~a packages" package-count)))))
    (connect previous-generation-opening-button clicked
      (lambda _
        (let ((iter (tree-model-iter-previous tree-store
                     (tree-view-current-generation-iter tree-view tree-store tree-filter-model))))
          (when iter
              (tree-view-select-generation tree-view tree-store iter tree-filter-model #t profile)))))
    (connect next-generation-opening-button clicked
      (lambda _
        (let ((iter (tree-model-iter-next tree-store
                     (tree-view-current-generation-iter tree-view tree-store tree-filter-model))))
          (when iter
              (tree-view-select-generation tree-view tree-store iter tree-filter-model #t profile)))))
    (box:pack-start box committal-button #t #t 0)
    (box:pack-start box package-installation-button #t #t 0)
    (box:pack-start box package-uninstallation-button #t #t 0)
    (box:pack-start box previous-generation-opening-button #t #t 0)
    (box:pack-start box next-generation-opening-button #t #t 0)
    box))

(define (tree-row-generation-number tree-model tree-iter)
  (string->number (tree-model-cell-value tree-model tree-iter gui-manifests:version-column-index)))

(define (profile-window-new profile)
  (let* ((window (window:new (symbol->window-type 'toplevel)))
         (panel (box:new (symbol->orientation 'horizontal) 0))
         (tree-store (gui-manifests:tree-store-new))
         (header-bar (header-bar:new))
         (generations (profiles:profile-generations profile))
         (current-generation-number (profiles:generation-number profile)))
    (let-values (((scrolled-window tree-view tree-filter-model)
                  (scrollable-tree-view-new-with-simple-model tree-store #f "Package" gui-manifests:display-name-column-index gui-manifests:tooltip-column-index)))
      (connect tree-view test-expand-row
        (lambda (tree-view iter path)
          (let* ((store-iter (tree-filter-iter->store-iter tree-filter-model iter)))
            (idle-add
             G_PRIORITY_DEFAULT_IDLE
             (lambda _
              (catch #t
               (lambda ()
                  (tree-store-expand-generation tree-store store-iter profile)
                  (write "OK")
                  (newline)
                  #f) ; don't call me again
               (lambda error
                 (write error)
                 (newline))))
             #f)
            #f))) ; allow expansion
      (for-each (lambda (generation)
                  (let ((store-iter (gui-manifests:generation->tree-store-location! profile generation tree-store #f)))
                    (when (= generation current-generation-number)
                        (tree-view-select-generation tree-view tree-store store-iter tree-filter-model #f profile))))
                generations)
      (pack-start panel scrolled-window #t #t 0)
      (pack-start panel (profile-action-buttons-new profile tree-view tree-filter-model tree-store)
                        #f #f 7)
      (add window panel)
      (set! (show-close-button header-bar) #t)
      (set-titlebar window header-bar)
      (set-title window (format #f "Profile ~a" profile))
      ;(set-subtitle header-bar (format #f "(as of ~a)" generation-timestamp))
      (set-default-size window 600 400)
      window)))

(define (activate app)
  (let* ((profile profiles:%current-profile)
         ;(window (profile-generation-window-new profile (profiles:generation-number profile)))
         (window (profile-window-new profile))
         (dummy-window (application-window:new app)))
    (show-all dummy-window)
    (show-all window)))

(define-public (main)
  (let ((app (application:new "org.gnu.guix.ProfileViewer" (number->application-flags 0))))
    (connect app application:activate activate)
    (run app (command-line))))
