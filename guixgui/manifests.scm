;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (guixgui manifests)
  #:use-module ((guix packages) #:prefix packages:)
  #:use-module ((guix profiles) #:prefix profiles:)
  #:use-module ((gnu packages) #:prefix packages:)
  #:use-module ((guixgui texinfo) #:prefix texinfo:)
  #:use-module (srfi srfi-19) ; date->string
  #:use-module (srfi srfi-43) ; vector-map, vector-for-each
  #:use-module (gi)
  #:use-module (gi repository)
  #:use-module (gi types)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match))
;(use-modules (oop goops))

; Note: Or just (use-typelibs (("Gio" "2.0") #:renamer (protect 'application:new))
;;              ("Gtk" "3.0"))

;(use-typelibs ("GLib" "2.0"))

(eval-when (compile load eval)
  (require "GObject" "2.0")
  (load-by-name "GObject" "Object") ; https://github.com/spk121/guile-gi/issues/88
  (load-by-name "GObject" "Value") ; https://github.com/spk121/guile-gi/issues/97
  (require "GLib" "2.0")
  ;(require "GdkPixbuf" "2.0")
  (require "Gio" "2.0")
  (require "Gtk" "3.0")
  (load-by-name "Gio" "Application")
  (load-by-name "Gio" "ApplicationFlags")

  (load-by-name "GLib" "markup_escape_text")
  (for-each
   (cute load-by-name "Gtk" <>)
   '("TreeStore" "TreeModel" "TreeIter")))

;(define manifest-tree-model-columns '("name" "version" "output" "item"))

(define-public name-column-index 0)
(define-public version-column-index 1)
(define output-column-index 2)
(define item-column-index 3)
(define-public display-name-column-index 4)
(define-public tooltip-column-index 5)
(define-public package-column-index 6)

(define-public (package-representant name version)
  "Find a representant for package spec NAME@VERSION.
This is done in a low-overhead way--so it doesn't be exactly the correct
package, just enough to read synopsis, home-page, description, license."
  ; TODO: provenance (repository (version 0) (url "https://git.savannah.gnu.org/git/guix.git") (branch "master") (commit "767a0a18d88479c713f1b9b034bd06eedfe71a80"))
  (match (or (packages:find-packages-by-name name version)
             (packages:find-packages-by-name name))
   ((package rest ...)
    package)
   (_ #f)))

(define-public (package->tooltip package)
  (let ((name (texinfo:texinfo-markup->pango-markup (packages:package-name package)))
        (synopsis (texinfo:texinfo-markup->pango-markup (packages:package-synopsis package)))
        (description (texinfo:texinfo-markup->pango-markup (packages:package-description package)))
        (home-page (and=> (packages:package-home-page package) (lambda (x) (markup-escape-text x -1)))))
    (format #f "Name: ~a\nSynpopsis: ~a\n\nDescription:\n\n~a\n\nHome-page: ~a"
               name synopsis description home-page)))

(define (make-string-GValue)
  (value:init (make <GValue>) <string>))

(define-public (->tree-store-location! manifest destination-tree-store destination-parent-iter)
  "Store the entries of the MANIFEST into DESTINATION-TREE-STORE under DESTINATION-PARENT-ITER"
  (define column-count (tree-model:get-n-columns destination-tree-store))
  (define row (list->vector (map (lambda (index)
                                   (make-string-GValue))
                                 (iota column-count)))) ; no <package>
  (define iter (make <GtkTreeIter>))
  (for-each (lambda (entry)
              (let* ((name (profiles:manifest-entry-name entry))
                     (version (profiles:manifest-entry-version entry))
                     (output (profiles:manifest-entry-output entry))
                     (item (profiles:manifest-entry-item entry))
                     (package-spec (string-append name "@" version ":" output))
                     (display-name (match output
                                    ("out" (string-append name " " version))
                                    (_ package-spec)))
                     (package (package-representant name version)))
                (set! ((vector-ref row name-column-index) <string>) name)
                (set! ((vector-ref row version-column-index) <string>) version)
                (set! ((vector-ref row output-column-index) <string>) output)
                (set! ((vector-ref row item-column-index) <string>) item)
                (set! ((vector-ref row display-name-column-index) <string>) display-name)
                (set! ((vector-ref row tooltip-column-index) <string>) ""))
              (tree-store:append! destination-tree-store iter destination-parent-iter)
              ;(set! (value <string>) "hello")
              (tree-store:set destination-tree-store iter (apply u32vector (iota column-count)) row)) ; no <package>
            (profiles:manifest-entries manifest)))

(define (dummy-row->tree-store-location! destination-tree-store destination-parent-iter)
  (define iter (make <GtkTreeIter>))
  (tree-store:append! destination-tree-store iter destination-parent-iter)
  iter)

(define-public (generation->tree-store-location! profile generation-number destination-tree-store destination-parent-iter)
  "Store the generation (PROFILE GENERATION-NUMBER) into DESTINATION-TREE-STORE under DESTINATION-PARENT-ITER"
  (define column-count (tree-model:get-n-columns destination-tree-store))
  (define row (list->vector (map (lambda (index)
                                   (make-string-GValue))
                                 (iota column-count)))) ; no <package>
  (define iter (make <GtkTreeIter>))
  (let* ((generation-timestamp
          (date->string
           (time-utc->date
            (profiles:generation-time profile generation-number))))
         (display-name (format #f "#~a (~a)" generation-number generation-timestamp)))
    (set! ((vector-ref row display-name-column-index) <string>) display-name)
    (set! ((vector-ref row tooltip-column-index) <string>) "X")
    (set! ((vector-ref row name-column-index) <string>) "generation")
    (set! ((vector-ref row version-column-index) <string>) (number->string generation-number)))
  (tree-store:append! destination-tree-store iter destination-parent-iter)
  (dummy-row->tree-store-location! destination-tree-store iter)
  (tree-store:set destination-tree-store iter (apply u32vector (iota column-count)) row) ; no <package>
  iter)

(define column-types
  (vector <string> <string> <string> <string> <string> <string>)) ; <GClosure>

(define-public (tree-store-new)
  (tree-store:new column-types))

(define-public (tree-model-row model iter)
  (let ((row (vector-map (lambda (i class)
                           ; Gtk does that: (value:init (make <GValue>) class)
                           (make <GValue>))
                         column-types)))
    ;(tree-model:get! model iter (zip (iota (length row)) row))
    (vector-for-each
     (lambda (i x)
       (tree-model:get-value! model iter i x))
     row)
    row))

(define (get-available-packages)
  (define destination-tree-store (tree-store-new))
  (define column-count (tree-model:get-n-columns destination-tree-store))
  (define row (list->vector (map (lambda (index)
                                   (make <GValue>))
                                 (iota column-count)))) ; no <package>
  (define iter (make <GtkTreeIter>))
  (define column-indices (apply u32vector (iota column-count)))
  (let ((store destination-tree-store)
        (packages (packages:fold-packages cons '())))
    (for-each (lambda (package)
                (let* ((name (packages:package-name package))
                       (version (packages:package-version package))
                       (display-name name))
                  (set! ((vector-ref row name-column-index) <string>) name)
                  (set! ((vector-ref row version-column-index) <string>) version)
                  (set! ((vector-ref row output-column-index) <string>) "X")
                  (set! ((vector-ref row item-column-index) <string>) "X")
                  (set! ((vector-ref row display-name-column-index) <string>) display-name)
                  (set! ((vector-ref row tooltip-column-index) <string>) "")
                  ;(set! ((vector-ref row tooltip-column-index) <string>)
                  ; (or (and=> package package->tooltip) ""))
                  ;(set! ((vector-ref row package-column-index) <GClosure>) (procedure->closure (lambda () package)))
)
              (tree-store:append! destination-tree-store iter #f)
              ;(set! (value <string>) "hello")
              (tree-store:set destination-tree-store iter column-indices row))
              packages)
    store))

(define-public available-packages (get-available-packages))
