;;; Copyright © 2020 Danny Milosavljevic <dannym@scratchpost.org>
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Based on (texinfo html) of Guile 3.0.4.
;;;;    Copyright (C) 2009, 2010, 2011, 2020  Free Software Foundation, Inc.
;;;;    Copyright (C) 2003,2004,2009 Andy Wingo <wingo at pobox dot com>

(define-module (guixgui texinfo)
  #:use-module (gi)
  #:use-module (gi repository)
  #:use-module (gi types)
  #:use-module (texinfo)
  #:use-module (sxml simple)
  #:use-module (sxml transform)
  #:use-module (ice-9 match))

(eval-when (compile load eval)
  (require "GLib" "2.0")
  (load-by-name "GLib" "markup_escape_text"))

;; TODO: Use Pango Markup <big>, <s>(Strikethrough), <sub>(subscript), <sup>(superscript).
(define tag-replacements
  '((titlepage    span (@ (class "titlepage")))
    (title        span  (@ (class "title")))
    (subtitle     span  (@ (class "subtitle")))
    (author       span  (@ (class "author")))
    (example      tt)
    (lisp         tt)
    (smallexample tt) ; TODO: span smaller
    (smalllisp    tt) ; TODO: span smaller
    (cartouche    span (@ (class "cartouche")))
    (verbatim     tt)
    (chapter      b)
    (section      i)
    (subsection   u)
    (subsubsection       span (@ (underline "double")))
    (appendix     b)
    (appendixsec  i)
    (appendixsubsec      u)
    (appendixsubsubsec   span (@ (underline "double")))
    (unnumbered   b)
    (unnumberedsec       i)
    (unnumberedsubsec    u)
    (unnumberedsubsubsec span (@ (underline "double")))
    (majorheading b)
    (chapheading  i)
    (heading      u)
    (subheading   span)
    (subsubheading       span (@ (underline "double")))
    (quotation    small)
    (item         span) ; TODO make list item
    (para         span) ; TODO make paragraph
    (*fragment*   span)

    (asis         span)
    (w            span (@ (class "verbatim")))
    (bold         b)
    (i            i)
    (sample       tt)
    (samp         tt)
    (code         tt)
    (math         i)
    (kbd          i)
    (key          tt)
    (var          var)
    (env          tt)
    (file         tt)
    (command      tt)
    (option       tt)
    (url          span (@ (class "url")))
    (dfn          u)
    (cite         small)
    (acro         span)
    (email        span (@ (class "email")))
    (emph         i)
    (strong       b)
    (sc           span (@ (variant "small-caps")))))

(define ignore-list
  '(page setfilename setchapternewpage iftex ifinfo ifplaintext ifxml sp vskip
    menu ignore syncodeindex comment c dircategory direntry top shortcontents
    cindex printindex))

;(define (arg-ref key %-args)
;  (match %-args
;   (('% args ...)
;    (and=> (and=> (assq key args) cdr)
;           (lambda (x) (texinfo-markup->pango-markup (cdr x)))))))

(define (arg-ref key %-args)
  ;; TODO: texinfo-markup->pango-markup double-escapes!
  (and=> (assq key (cdr %-args)) (lambda (x) (identity (car (cdr x))))))

(define (arg-req key %-args)
  (or (arg-ref key %-args)
      (error "Missing argument:" key %-args)))

;; See <https://www.gnu.org/software/texinfo/manual/texinfo/html_node/_0040enumerate.html>.
;; TODO: Actually count up for the items.
(define enumerate
  (match-lambda
   ((enumerate ('% start ...) rest ...)
    (let ((tonumber
           (lambda (start)
    (let ((c (string-ref start 0)))
      (cond ((number? c) (string->number start))
            (else (1+ (- (char->integer c)
                         (char->integer (if (char-upper-case? c) #\A #\a))))))))))

    (cons `(@ (start ,@(tonumber (arg-req 'start rest))))
                    rest)))
   ((enumerate x ...)
    x)))

(define (enumerate tag . elts)
  `(span ,@(match elts
            ;; "@enumerate 3"
            ((('% start ...) rest ...)
             (cons `(@ (start ,@(tonumber (arg-req 'start rest))))
                   rest))
            (x x))))

(define (itemize tag . elts)
  `(span ,@(match elts
           ;; Strip `bullet' attribute.
           ((('% . attrs) . elts) elts)
           (elts elts))))

(define (acronym tag . elts)
  (match elts
    ;; FIXME: Need attribute matcher that doesn't depend on attribute
    ;; order.
    ((('% ('acronym text) . _)) `(i ,text))))

(define (uref tag %-args)
  ;(write "%-args")
  ;(write %-args)
  ;(newline)
  (let ((url (arg-req 'url %-args))
        (title (arg-ref 'title %-args)))
    (if title
        (string-append title " <" url ">")
        (string-append "<" url ">"))))

(define (entry tag %-args . body)
  (let ((heading (arg-req 'heading %-args)))
    (cons* "\n" `(tt ,heading) ": " (apply append body))))

(define (table tag %-args . body)
  (let ((formatter (arg-req 'formatter %-args)))
    (match formatter
     (('code) `("\n" (tt ,(apply append body)))) ; TODO: maybe no <tt> here--just in the entries?
     (('asis) `("\n" ,(apply append body))) ; TODO: No texinfo markup
     ;; TODO: ('command)
     (_ `("\n" ,(apply append body))))))

;; TODO: Guix actually uses, and we don't handle,:
;; *braces*

(define-public (texinfo-markup->pango-markup text)
  (define rules
    `((% *preorder* . ,(lambda args args))
    ;(copyright . ,(lambda args '(*ENTITY* "copy")))
    ;(result    . ,(lambda args '(*ENTITY* "rArr")))
    (tie       . ,(lambda args "\xC2\xA0"))
    (dots      . ,(lambda args "..."))
    ;(xref . ,ref) (ref . ,ref) (pxref . ,ref)
    (uref . ,uref)
    ;(node . ,node) (anchor . ,node)
    (table . ,table)
    (enumerate . ,enumerate)
    (itemize . ,itemize)
    (acronym . ,acronym)
    (entry . ,entry)

    ;(deftp . ,def) (defcv . ,def) (defivar . ,def) (deftypeivar . ,def)
    ;(defop . ,def) (deftypeop . ,def) (defmethod . ,def)
    ;(deftypemethod . ,def) (defopt . ,def) (defvr . ,def) (defvar . ,def)
    ;(deftypevr . ,def) (deftypevar . ,def) (deffn . ,def)
    ;(deftypefn . ,def) (defmac . ,def) (defspec . ,def) (defun . ,def)
    ;(deftypefun . ,def)
    ;(ifnottex . ,(lambda (tag . body) body))

    (*text* . ,(lambda (tag text) (markup-escape-text text -1)))
    (*default* . ,(lambda (tag . tail)
                    (match (assq tag tag-replacements)
                     ((tag replacement-tag attributes ...)
                      (cons (cons replacement-tag attributes) tail))
                     (_
                      (warn "Don't know how to convert" tag "to Pango markup")
                      (write tail)
                      (newline)
                      `(span "[?]")))))))

  ;(markup-escape-text text -1)
  (let* ((result (pre-post-order (texi-fragment->stexi text) rules)))
    (when (string-contains (sxml->string result) "&amp;&")
        (display "POSSIBLY BROKEN\n")
        (write (call-with-output-string
     (lambda (port)
       (sxml->xml result port))))
        (newline))
    (call-with-output-string
     (lambda (port)
       (sxml->xml result port)))))
