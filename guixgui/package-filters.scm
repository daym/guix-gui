;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (guixgui package-filters)
  #:use-module ((guix packages) #:prefix packages:)
  #:use-module ((gnu packages) #:prefix packages:)
  #:use-module ((guixgui manifests) #:prefix gui-manifests:)
  #:use-module (gi)
  #:use-module (gi repository)
  #:use-module (gi types)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-43) ; vector-map, vector-for-each
  #:use-module (ice-9 match)
  #:use-module (oop goops))

(use-typelibs (("Gtk" "3.0") #:select (symbol->window-type symbol->orientation symbol->sort-type symbol->selection-mode changed)))

(eval-when (compile load eval)
  (require "GObject" "2.0")
  (load-by-name "GObject" "Object") ; https://github.com/spk121/guile-gi/issues/88
  (load-by-name "GObject" "Value")
  (load-by-name "GObject" "Closure")
  (require "GLib" "2.0")
  ;(require "GdkPixbuf" "2.0")
  (require "Gio" "2.0")
  (require "Gtk" "3.0")
  (load-by-name "Gio" "Application")
  (load-by-name "Gio" "ApplicationFlags")

  (load-by-name "GLib" "markup_escape_text")
  (for-each
   (cute load-by-name "Gtk" <>)
   '("TreeStore" "TreeModel" "TreeIter" "Box" "Label" "Widget" "CheckButton" "Entry" "Editable" "ToggleButton" "CheckButton" "Container" "Expander" "TreeModelFilter")))

;;; FIXME: <GClosure>
;;(define <FilterCondition>
;;  (register-type "FilterCondition" <GtkBox> (list (param-spec-closure "predicate") #f)))
;; On getting "predicate": 

; ParamSpec boxed
; GParamSpec* g_object_class_find_property (GObjectClass *oclass, const gchar ... void g_object_watch_closure (GObject *object, GClosure *closure);.

(define filters
 `((#f "Is a graphical program"
    #f "Is a non-graphical program"
    ()
    ,(lambda (package)
       #t))
   (#f "Has license"
    #f "Does not have license"
    ((one-of "a" "b" "c"))
    ,(lambda (package license)
       #t))
   (#f "Has dependency"
    #f "Does not have dependency"
    (package)
    ,(lambda (package dependency)
       #t))
   (#f "Is deprecated"
    #t "Is not deprecated"
    ()
    ,(lambda (package)
       (and (packages:package-superseded package) #t)))
   (#f "Is hidden"
    #t "Is not hidden"
    ()
    ,packages:hidden-package?)
   (#f "Name contains"
    #f "Name does not contain"
    (text)
    ,(lambda (package text)
       (and (string-contains (or (package-name package) "") text) #t)))
   (#f "Synopsis contains"
    #f "Synopsis does not contain"
    (text)
    ,(lambda (package text)
       (and (string-contains (or (package-synopsis package) "") text) #t)))
   (#f "Description contains"
    #f "Description does not contain"
    (text)
    ,(lambda (package text)
       (and (string-contains (or (package-description package) "") text) #t)))))

(define (gui-row-filter-procedure panel)
  (fold (lambda (gui-cell result)
          (gui-row-filter-procedure panel)
          (lambda (package)
            (and package result)))
        (lambda (package)
          #t)
        (container:get-children panel)))

(define (filter-procedure panel)
  (fold (lambda (gui-row result)
          (lambda (package)
            (and package result)))
        (lambda (package)
          #t)
        (container:get-children panel)))

;; XXX: dupe
(define (tree-model-cell-value model iter index)
  (let ((result (make <GValue>)))
    (tree-model:get-value! model iter index result)
    (result)))

(define-public (package-filters tree-model-filter)
  "Create filter value widgets and returns them, and install a visible-func
in TREE-MODEL-FILTER that checks the filter widgets."
  (let* ((outer-panel (box:new (symbol->orientation 'vertical) 0))
         (outer-entry (entry:new))
         (expander (expander:new-with-mnemonic "_Filters"))
         (panel (box:new (symbol->orientation 'vertical) 7))
         (with-change-notification
          (lambda (widget signal-name)
            (connect widget (make <signal> #:name signal-name)
                     (lambda args
                       (write "CHANGED\n")
                       (force-output)
                       ;(write (map (filter-procedure panel) available-packages))
                       (tree-model-filter:refilter tree-model-filter)))
            widget))
         (with-change-notification*
          (lambda (check-button widget signal-name)
            "On change of WIDGET value, set CHECK-BUTTON to checked and then filter the tree view."
            (connect widget (make <signal> #:name signal-name)
                     (lambda args
                       (set! (active check-button) #t)
                       (write "CHANGED\n")
                       (force-output)
                       (tree-model-filter:refilter tree-model-filter)))
            widget))
         (package-filter-new
          (lambda (default-checked title parameters filter)
            (let ((panel (box:new (symbol->orientation 'horizontal) 7))
                  (check-button (with-change-notification (check-button:new-with-label title) "toggled")))
              (set! (active check-button) default-checked)
              (pack-start panel check-button #f #f 7)
              (for-each
               (match-lambda
                ('package
                 (pack-start panel (with-change-notification* check-button (entry:new) "changed") #f #f 7))
                ('text
                 (pack-start panel (with-change-notification* check-button (entry:new) "changed") #f #f 7))
                (('one-of rest ...)
                 (pack-start panel (with-change-notification* check-button (entry:new) "changed") #f #f 7)))
               parameters)
              panel))))
    (pack-start outer-panel (with-change-notification outer-entry "changed") #f #f 7)
    (pack-start outer-panel expander #f #f 0)
    (add expander panel)
    (for-each
     (match-lambda
      ((default-checked-normal title default-checked-inverse inverted-title parameters filter)
       (pack-start panel (package-filter-new default-checked-normal title parameters filter) #f #f 0)
       (pack-start panel (package-filter-new default-checked-inverse inverted-title parameters filter) #f #f 0)))
     filters)
    (tree-model-filter:set-visible-func tree-model-filter
      (lambda (model iter user-data)
        (let* ((outer-entry-text (entry:get-text outer-entry))
               (tree-model-row (vector-map (lambda (i cell-value) (cell-value)) (gui-manifests:tree-model-row model iter)))
               (name (vector-ref tree-model-row gui-manifests:name-column-index))
               (version (vector-ref tree-model-row gui-manifests:version-column-index))
               (display-name (vector-ref tree-model-row gui-manifests:display-name-column-index)))
              ;; OUTPUT, ITEM, SYNOPSIS, DESCRIPTION ignored.
        (and
         (and #t ; FIXME: (filter-procedure panel)
              (or (string-contains (or name "") outer-entry-text)
                  (string-contains (or version "") outer-entry-text)
                  (string-contains (or display-name "") outer-entry-text)))
         #t))))
    (show-all outer-panel)
    outer-panel))

; TODO: packages:find-packages-by-name name version
