;;; Guix-gui --- GUI for Guix.
;;; Copyright (C) 2019 Jan Nieuwenhuizen <janneke@gnu.org>
;;; Copyright (C) 2020 Danny Milosavljevic <dannym@scratchpost.org>
;;;
;;; This file is part of guix-gui.
;;;
;;; guix-gui is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guix-gui is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-gui.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; GNU Guix development package.  To build and play, run:
;;
;;   guix environment --ad-hoc -l guix.scm guile
;;
;; To build and install, run:
;;
;;   guix package -f guix.scm
;;
;; To build it, but not install it, run:
;;
;;   guix build -f guix.scm
;;
;; To use as the basis for a development environment, run:
;;
;;   guix environment -l guix.scm
;;
;;; Code:

(use-modules (guix packages)
             (guix licenses)
             (guix git-download)
             (guix gexp)
             (guix build-system gnu)
             (guix build-system glib-or-gtk)
             (guix utils)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages gettext)
             (gnu packages glib)
             (gnu packages gnome)
             (gnu packages gtk)
             (gnu packages guile)
             (gnu packages package-management)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (ice-9 match))

(define %source-dir (dirname (current-filename)))

(define guile-gi
  (package
    (name "guile-gi")
    (version "git")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/spk121/guile-gi.git")
                    (commit "f0ab159ef0cb60bc3903dab867e137267e0c13a2")))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0kyap9fz15i5pwvh8q1x32v8hbflvxzdkwdl1j9pz3fds30p3qbl"))))
    (build-system glib-or-gtk-build-system)
    (native-inputs `(("autoconf" ,autoconf)
                     ("automake" ,automake)
                     ("gettext" ,gnu-gettext)
                     ("glib:bin" ,glib "bin")
                     ("libtool" ,libtool)
                     ("pkg-config" ,pkg-config)
                     ("texinfo" ,texinfo)))
    (inputs `(("guile" ,guile-3.0-latest)
              ("glib" ,glib)))
    (propagated-inputs `(("gobject-introspection" ,gobject-introspection)))
    (arguments
     `(#:configure-flags '("--with-gnu-filesystem-hierarchy"
                           "--enable-hardening")
       #:modules ((guix build glib-or-gtk-build-system)
                  (guix build utils)
                  (ice-9 popen)
                  (ice-9 rdelim))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-references-to-extension
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((effective (read-line
                               (open-pipe* OPEN_READ
                                           "guile" "-c"
                                           "(display (effective-version))"))))
               (substitute* '("module/gi.scm"
                              "module/gi/oop.scm"
                              "module/gi/documentation.scm"
                              "module/gi/types.scm"
                              "module/gi/repository.scm")
                 (("\\(load-extension \"libguile-gi\" \"(.*)\"\\)" m arg)
                  (format #f "~s"
                          `(load-extension
                            (format #f "~alibguile-gi"
                                    (if (getenv "GUILE_GI_UNINSTALLED")
                                        ""
                                        ,(format #f "~a/lib/guile/~a/extensions/"
                                                 (assoc-ref outputs "out")
                                                 effective)))
                            ,arg)))))
             (setenv "GUILE_GI_UNINSTALLED" "1")
             #t)))))
    (home-page "https://github.com/spk121/guile-gi")
    (synopsis "GObject bindings for Guile")
    (description
     "Guile-GI is a library for Guile that allows using GObject-based
libraries, such as GTK+3.  Its README comes with the disclaimer: This
is alpha code.")
    (license gpl3+)))

(define-public guix-gui
  (package
    (name "guix-gui")
    (version "0.1")
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (build-system glib-or-gtk-build-system)
    (arguments
     `(#:modules ((guix build utils)
                  (guix build glib-or-gtk-build-system)
                  (ice-9 rdelim)
                  (ice-9 popen))
       #:phases
       (modify-phases %standard-phases
           (add-after 'install 'wrap-program
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out    (assoc-ref outputs "out"))
                      (guile-gi (assoc-ref inputs "guile-gi"))
                      (guix   (assoc-ref inputs "guix"))
                      (deps   (list guile-gi guix))
                      (guile  (assoc-ref %build-inputs "guile"))
                      (effective (read-line
                                  (open-pipe* OPEN_READ
                                              (string-append guile "/bin/guile")
                                              "-c" "(display (effective-version))")))
                      (mods   (string-drop-right  ;drop trailing colon
                               (string-join deps
                                            (string-append "/share/guile/site/"
                                                           effective ":")
                                            'suffix)
                               1))
                      (objs   (string-drop-right
                               (string-join deps
                                            (string-append "/lib/guile/" effective
                                                           "/site-ccache:")
                                            'suffix)
                               1)))
                 (wrap-program (string-append out "/bin/guix-gui")
                   `("PATH" ":" prefix (,(string-append out "/bin")))
                   `("GUILE_LOAD_PATH" ":" prefix (,mods))
                   `("GUILE_LOAD_COMPILED_PATH" ":" prefix (,objs)))
                 #t))))))
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("gettext" ,gnu-gettext)
       ("guile" ,guile-3.0-latest)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("guile-gi" ,guile-gi)
       ("guix" ,guix)
       ("cairo" ,cairo)
       ("gtk+" ,gtk+)))
    (synopsis "Graphical frontend for Guix")
    (description "@code{guix-gui} uses Guile-GI to provide a Gtk-based
user interface for Guix.  It supports…")
    (home-page "https://gitlab.com/daym/guix-gui")
    (license gpl3+)))

guix-gui
